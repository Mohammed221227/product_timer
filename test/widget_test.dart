// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:product_time_zone/main.dart';

void main() {
  // test('Test time zonr', () {
  //   final client = MockClient();
  //
  //
  //   expect(fetchAlbum(client), throwsException);
  // });

  DateTime productTime = DateTime.parse("2021-08-04T08:48:55.968Z");
  DateTime currentTime = getCurrentDateTime();
  Duration offset = currentTime.timeZoneOffset;
  DateTime realTime = productTime.add(offset);
  //---------------------------
  print("Current time ${currentTime.toIso8601String()}");
  print("Product time ${productTime.toIso8601String()}");
  print("Real time ${realTime.toIso8601String()}");

  // print(realTime.difference(productTime).inHours);

  print(currentTime.difference(realTime).inSeconds);


  const oneSec = const Duration(seconds: 1);
  new Timer.periodic(oneSec, (Timer timer) {
    DateTime _currentTime = getCurrentDateTime();

    print(_currentTime.difference(realTime).inSeconds);
  });
}

DateTime getCurrentDateTime() {
  return DateTime.now();
}
