import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget appLoadingOverLay() {
  return Container(
      width: Get.width,
      height: Get.height,
      decoration: BoxDecoration(
          border: Border.all(color: const Color(0xff979797), width: 1),
          boxShadow: [
            BoxShadow(
                color: const Color(0x80d7d7d7),
                offset: Offset(0, 1),
                blurRadius: 1,
                spreadRadius: 0)
          ],
          color: Colors.black.withOpacity(0.1)),
      child: Center(child: CircularProgressIndicator()));
}
