import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../res.dart';

Widget emptyData({@required String message}) {
  return Column(
    children: [
      Image.asset(
        Res.empty_data,
        width: Get.width / 3,
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          message,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      )
    ],
  );
}
