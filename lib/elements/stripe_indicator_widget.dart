import 'package:flutter/cupertino.dart';

Widget stripeIndicator() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                width: 40,
                height: 5.75,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12.5)),
                    color: const Color(0xffdadada))),
          ],
        ),
      ),
      // SizedBox(
      //   height: 5,
      // )
    ],
  );
}
