import 'package:flutter/material.dart';

import 'package:product_time_zone/elements/spacer.dart';
import 'package:product_time_zone/utils/hex_color.dart';

Widget textFiled(
    {@required String hintText,
    bool isPassword = false,
    bool showPassword = false,
    Function onTogglePasswordViability,
    bool isValidPhone = false,
    TextEditingController ctrl,
    TextInputType textInputType = TextInputType.text,
    Function validation,
    bool autoFocus = false,
    previewText = ""}) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      color: const Color(0xfff4f5fa),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child: Text("Phone Number",
        //       style: const TextStyle(
        //           color: const Color(0xff3a211b),
        //           fontWeight: FontWeight.w600,
        //           fontFamily: "SFProDisplay",
        //           fontStyle: FontStyle.normal,
        //           fontSize: 11.3),
        //       textAlign: TextAlign.center),
        // ),
        TextFormField(
          obscureText: isPassword && !showPassword,
          controller: ctrl,
          validator: validation,
          autofocus: autoFocus,
          keyboardType: textInputType,
          decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(8),
              hintText: hintText,
              // labelText: title,
              hintStyle: TextStyle(color: Colors.grey),
              suffix: isValidPhone
                  ? Image.asset(
                      "asset/images/check.png",
                      width: 17,
                      height: 17,
                    )
                  : SizedBox(),
              labelStyle: TextStyle(
                fontWeight: FontWeight.bold,
                color: const Color(0xff3a211b),
              )),
        ),
      ],
    ),
  );
}

Widget searchTextFiled({@required String hintText, Function onChange}) {
  return Container(
    // width: MediaQuery.of(context).size.width/1.1,
    decoration: BoxDecoration(
        color: Colors.white, borderRadius: BorderRadius.circular(5)),

    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        onChanged: onChange,
        decoration: InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.all(12),
            hintStyle: TextStyle(
              color: Color(0xffa4a4a4),
            ),
            hintText: hintText,
            prefixIcon: Icon(
              Icons.search,
              color: Color(0xffaeafb0),
            )),
      ),
    ),
  );
}

Widget noBorderTextFiled(
    {@required String hintText,
    TextEditingController controller,
    bool autoFocus = false,
    bool enabled = true,
    Function onChange,
    TextInputType textInputType = TextInputType.text,
    Function validation}) {
  return Container(
    // width: MediaQuery.of(context).size.width/1.1,

    child: TextFormField(
      controller: controller,
      autofocus: autoFocus,
      keyboardType: textInputType,
      validator: validation,
      onChanged: onChange,
      decoration: InputDecoration(
        enabled: enabled,
        border: InputBorder.none,
        contentPadding: EdgeInsets.symmetric(horizontal: 12),
        hintStyle: TextStyle(color: Color(0xffa4a4a4), fontSize: 14),
        hintText: hintText,
      ),
    ),
  );
}

//autoFocus
Widget noBorderTextArea(
    {@required String hintText,
    TextEditingController controller,
    bool autoFocus = true,
    Function validation,
    int maxLine = 5,
    int minLine = 3}) {
  return Container(
    // width: MediaQuery.of(context).size.width/1.1,

    child: TextFormField(
      controller: controller,
      keyboardType: TextInputType.multiline,
      validator: validation,
      minLines: minLine,
      autofocus: autoFocus,
      //Normal textInputField will be displayed
      maxLines: maxLine,
      // when user presses enter it will adapt to it
      decoration: InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.all(12),
        hintStyle: TextStyle(
          color: Color(0xffa4a4a4),
        ),
        hintText: hintText,
      ),
    ),
  );
}

Widget textFiledWithBottomBorder(
    {@required String hintText,
    @required String title,
    TextEditingController controller}) {
  return Container(
    // width: MediaQuery.of(context).size.width/1.1,

    child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          labelText: title,
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: HexColor("#b7b7b7")),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: HexColor("#b7b7b7")),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: HexColor("#b7b7b7")),
          ),
          contentPadding: EdgeInsets.all(12),
          labelStyle: TextStyle(color: Colors.grey),
          hintStyle: TextStyle(
            color: Color(0xffa4a4a4),
          ),
          hintText: hintText,
        ),
      ),
    ),
  );
}

Widget textFiledFullBorder(
    {@required String hintText,
    @required TextEditingController ctrl,
    TextInputType textInputType = TextInputType.text,
    Widget leadingSuffix,
    Function onPrefixClicked,
    Function validation,
    previewText = ""}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      decoration: BoxDecoration(
          // color: !isPreview ? HexColor("#EEEEEE") : Colors.white,
          border: Border.all(color: HexColor("#EEEEEE")),
          borderRadius: BorderRadius.circular(25)),
      child: TextFormField(
        controller: ctrl,
        validator: validation,
        keyboardType: textInputType,
        decoration: InputDecoration(
          suffixIcon: InkWell(onTap: onPrefixClicked, child: Icon(Icons.send)),
          contentPadding: EdgeInsets.all(8),
          border: InputBorder.none,
          hintText: hintText,
          hintStyle: TextStyle(color: Colors.grey),
        ),
      ),
    ),
  );
}

Widget textFiledV2(
    {@required String hintText,
    @required TextEditingController ctrl,
    TextInputType textInputType = TextInputType.text,
    Widget leadingSuffix,
    String title,
    Function onPrefixClicked,
    Function validation,
    previewText = ""}) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      decoration: BoxDecoration(
          // color: !isPreview ? HexColor("#EEEEEE") : Colors.white,
          border: Border.all(color: Colors.grey, width: 0.2),
          boxShadow: [
            BoxShadow(
                color: const Color(0x80c3c3c3),
                offset: Offset(0, 3),
                blurRadius: 3,
                spreadRadius: 0)
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            controller: ctrl,
            validator: validation,
            keyboardType: textInputType,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 16),
              border: InputBorder.none,
              hintText: hintText,
              hintStyle: TextStyle(color: Colors.grey),
            ),
          ),
          title != null
              ? Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text(title,
                      style: const TextStyle(
                          color: const Color(0xff000000),
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: 14.0)),
                )
              : SizedBox(
                  width: 0,
                ),
          title != null
              ? spacerH(10)
              : SizedBox(
                  width: 0,
                ),
        ],
      ),
    ),
  );
}

String requiredValidation(String data) {
  if (data.isEmpty)
    return "Required";
  else
    return null;
}

getTextFromCtrl(TextEditingController textEditingController) {
  return textEditingController.text.trim();
}
