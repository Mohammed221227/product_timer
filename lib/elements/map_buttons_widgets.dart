import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget mapButtonsWidgets(
    {@required Function onToMyLocation, @required Function onChangeMapType}) {
  return Align(
    alignment: Alignment.bottomRight,
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            width: 45,
            height: 45,
            child: FloatingActionButton(
              heroTag: "btn1",
              backgroundColor: Colors.white,
              child: Image.asset("asset/images/change_map.png"),
              onPressed: onChangeMapType,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            width: 45,
            height: 45,
            child: FloatingActionButton(
              heroTag: "btn2",
              backgroundColor: Colors.white,
              child: Image.asset("asset/images/to_my_location.png"),
              onPressed: onToMyLocation,
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    ),
  );
}
