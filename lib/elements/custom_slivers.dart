// import 'dart:math';
//
// import 'package:flutter/material.dart';
//
// class SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
//   SliverAppBarDelegate({
//     @required this.minHeight,
//     @required this.maxHeight,
//     @required this.child,
//   });
//   final double minHeight;
//   final double maxHeight;
//   final Widget child;
//   @override
//   double get minExtent => minHeight;
//   @override
//   double get maxExtent => max(maxHeight, minHeight);
//   @override
//   Widget build(
//       BuildContext context, double shrinkOffset, bool overlapsContent) {
//     return new SizedBox.expand(child: child);
//   }
//
//   @override
//   bool shouldRebuild(SliverAppBarDelegate oldDelegate) {
//     return maxHeight != oldDelegate.maxHeight ||
//         minHeight != oldDelegate.minHeight ||
//         child != oldDelegate.child;
//   }
// }
//
// class HomePage extends StatefulWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }
//
// class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
// //Global Keys for all widgets in our page
//   final greenKey = new GlobalKey();
//   final blueKey = new GlobalKey();
//   final orangeKey = new GlobalKey();
//   final yellowKey = new GlobalKey();
//   ScrollController scrollController;
//   TabController _tabController;
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     scrollController = ScrollController();
//     _tabController = new TabController(length: 4, vsync: this);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(),
//       body: CustomScrollView(
//         controller: scrollController,
//         slivers: <Widget>[
//           SliverList(
//             delegate: SliverChildListDelegate(
//               [
//                 Container(
//                   height: 200,
//                   color: Colors.black,
//                 ),
//               ],
//             ),
//           ),
//           makeTabBarHeader(),
//           SliverList(
//             delegate: SliverChildListDelegate(
//               [
//                 Container(
//                   key: greenKey,
//                   height: 800,
//                   color: Colors.green,
//                 ),
//                 Container(
//                   key: blueKey,
//                   height: 800,
//                   color: Colors.blue,
//                 ),
//                 Container(
//                   key: orangeKey,
//                   height: 800,
//                   color: Colors.orange,
//                 ),
//                 Container(
//                   key: yellowKey,
//                   height: 800,
//                   color: Colors.yellow,
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   SliverPersistentHeader makeTabBarHeader() {
//     return SliverPersistentHeader(
//       pinned: true,
//       delegate: SliverAppBarDelegate(
//         minHeight: 50.0,
//         maxHeight: 50.0,
//         child: Container(
//           color: Colors.white,
//           child: TabBar(
//             onTap: (val) {
//               switch (val) {
//                 case 0:
//                   {
//                     scrollController.position.ensureVisible(
//                       greenKey.currentContext.findRenderObject(),
//                       alignment: 0.0,
//                       // How far into view the item should be scrolled (between 0 and 1).
//                       duration: const Duration(milliseconds: 300),
//                     );
//                   }
//                   break;
//                 case 1:
//                   {
//                     scrollController.position.ensureVisible(
//                       blueKey.currentContext.findRenderObject(),
//                       alignment: 0.0,
//                       duration: const Duration(milliseconds: 300),
//                     );
//                   }
//                   break;
//                 case 2:
//                   {
//                     scrollController.position.ensureVisible(
//                       orangeKey.currentContext.findRenderObject(),
//                       alignment: 0.0,
//                       duration: const Duration(milliseconds: 300),
//                     );
//                   }
//                   break;
//                 case 3:
//                   {
//                     scrollController.position.ensureVisible(
//                       yellowKey.currentContext.findRenderObject(),
//                       alignment: 0.0,
//                       duration: const Duration(milliseconds: 300),
//                     );
//                   }
//                   break;
//               }
//             },
//             unselectedLabelColor: Colors.grey.shade700,
//             indicatorColor: Colors.red,
//             indicatorWeight: 2.0,
//             labelColor: Colors.red,
//             controller: _tabController,
//             tabs: <Widget>[
//               new Tab(
//                 child: Text(
//                   "Green",
//                   style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
//                 ),
//               ),
//               new Tab(
//                 child: Text(
//                   "Blue",
//                   style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
//                 ),
//               ),
//               new Tab(
//                 child: Text(
//                   "Orange",
//                   style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
//                 ),
//               ),
//               new Tab(
//                 child: Text(
//                   "Yellow",
//                   style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
//                 ),
//               ),
//             ],
//             indicatorSize: TabBarIndicatorSize.tab,
//           ),
//         ),
//       ),
//     );
//   }
// }
