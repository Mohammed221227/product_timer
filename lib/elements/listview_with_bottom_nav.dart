import 'package:flutter/material.dart';

//LLgRi7cWeUUu5zpB7dekZFapB2XdRCvM4N
Widget listViewWithBottomNav(
    {List<Widget> children, Widget bottomNavigation, shrink_wrap = true,ctrl}) {
  return Column(
    children: <Widget>[
      Expanded(
        child: ListView(
          controller: ctrl,

          // physics: NeverScrollableScrollPhysics(),
          shrinkWrap: shrink_wrap,
          children: children,
        ),
      ),
      Container(
        // This align moves the children to the bottom
          child: Align(
              alignment: FractionalOffset.bottomCenter,
              // This container holds all the children that will be aligned
              // on the bottom and should not scroll with the above ListView
              child: bottomNavigation))
    ],
  );
}
//listViewWithBottomNavNoTransparent
Widget listViewWithBottomNavNoTransparent(
    {List<Widget> children, Widget bottomNavigation, shrink_wrap = false}) {
  return Column(
    children: <Widget>[
      Expanded(
        child: ListView(
          shrinkWrap: shrink_wrap,
          children: children,
        ),
      ),
      Container(
        // This align moves the children to the bottom
          child: Align(
              alignment: FractionalOffset.bottomCenter,
              // This container holds all the children that will be aligned
              // on the bottom and should not scroll with the above ListView
              child: bottomNavigation))
    ],
  );
}