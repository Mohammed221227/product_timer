import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:product_time_zone/core/config/app_colors.dart';

double btnSize = 50;

Widget appBtn(
    {@required Function onTap,
    String text,
    @required double w,
    @required double h,
    bool isDisabled = false,
    Color color,
    Widget child}) {
  return InkWell(
    child: Container(
        width: w,
        height: h ?? btnSize,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: !isDisabled
                ? color == null
                    ? AppColors.primaryColor
                    : color
                : Colors.grey),
        child: Center(
          child: child == null
              ? Text(text,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      fontSize: 17.8))
              : child,
        )),
    onTap: !isDisabled ? onTap : null,
  );
}
