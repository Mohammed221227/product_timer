import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';

Widget datePickerItem(
    {@required String title,
    @required DateTime dateTime,
    @required Function onDatePicked}) {
  return InkWell(
    onTap: () async {
      DatePicker.showDateTimePicker(Get.context, showTitleActions: true,
          onChanged: (date) {
        print('change $date');
      }, onConfirm: (date) {
        onDatePicked(date);
      });
    },
    child: Container(
      width: Get.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: const Color(0xfff4f5fa),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
                dateTime == null ? "Pick $title" : dateTime.toIso8601String()),
          )
        ],
      ),
    ),
  );
}
