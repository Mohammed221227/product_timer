import 'package:flutter/material.dart';

showAppDilaog(
    {@required String message , @required BuildContext ctx, @required Function onBack}) {
  Widget okBtn = FlatButton(child: Text("Ok"), onPressed: onBack);

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Message"),
    content: Text(message),
    actions: [
      okBtn,
    ],
  );

  // show the dialog
  showDialog(
    context: ctx,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
