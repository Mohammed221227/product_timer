import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget emptyDataIndicator({@required String message}) {
  return Padding(
    padding: const EdgeInsets.all(18.0),
    child: Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Image.asset(
          "asset/images/empty.png",
          width: 100,
          height: 100,
        ),
        SizedBox(
          height: 20,
        ),
        SizedBox(
          width: Get.width / 1.8,
          child: Text(
            message,
            style: TextStyle(fontSize: 17, color: Colors.grey),
            textAlign: TextAlign.center,
          ),
        )
      ],
    ),
  );
}
