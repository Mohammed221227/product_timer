import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:product_time_zone/core/config/app_colors.dart';
import 'package:product_time_zone/model/product_model.dart';
import 'package:product_time_zone/utils/date_helper.dart';

class ProductItem extends StatefulWidget {
  final ProductModel productModel;

  ProductItem({@required this.productModel});

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  String remainingDateToStart = "";
  String remainingDateToEnd = "";

  @override
  void initState() {
    super.initState();
    updateData();
    new Timer.periodic(Duration(minutes: 1), (Timer timer) async {
      updateData();
    });
  }

  updateData() async {
    DateTime currentTime = await AppDateHelper.currentDateTime();

    Duration start =
        await AppDateHelper.getDiffranceBetweenStartDateAndCurrentTime(
            widget.productModel,
            cuurentTime: currentTime);
    print("Start-------------> ${start}");
    Duration end = await AppDateHelper.getDiffranceBetweenEndDateAndCurrentTime(
        widget.productModel,
        currentTime: currentTime);
    print("Start-------------> ${end}");

    setState(() {
      remainingDateToStart = "${AppDateHelper.getFormatedDate(start)}";
      remainingDateToEnd = "${AppDateHelper.getFormatedDate(end)}";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: Get.width,
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(widget.productModel.productName),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  decoration: BoxDecoration(
                      color: AppColors.primaryColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Start in: $remainingDateToStart ",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  decoration: BoxDecoration(
                      color: AppColors.primaryColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "End in: $remainingDateToEnd ",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
