class ProductModel {
  String productName;
  String startAt;
  String endAt;

  ProductModel({this.productName, this.startAt, this.endAt});

  ProductModel.fromJson(Map<dynamic, dynamic> json) {
    productName = json['product_name'];
    startAt = json['start_at'];
    endAt = json['end_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_name'] = this.productName;
    data['start_at'] = this.startAt;
    data['end_at'] = this.endAt;
    return data;
  }
}
