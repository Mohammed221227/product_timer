abstract class TypeMapper<From, To> {
  To map(From from);
}
