import 'dart:ui';

import 'package:product_time_zone/utils/hex_color.dart';


class AppColors {
  static final primaryColor = HexColor("#00B199");
  static final lightYellow = Color(0xfffde140);

  static var brown = Color(0xff000000);

  static var lightBrown = Color(0xffa28f88);
}
