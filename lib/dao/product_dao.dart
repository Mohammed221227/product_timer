import 'package:firebase_database/firebase_database.dart';
import 'package:product_time_zone/model/product_model.dart';

class ProductDao {
  final DatabaseReference _messagesRef =
      FirebaseDatabase.instance.reference().child('products');

  Future<void> saveProduct(ProductModel productModel) async {
    await _messagesRef.push().set(productModel.toJson());
  }

  Query getProductQuery() {
    return _messagesRef;
  }
}
