import 'package:flutter/cupertino.dart';
import 'package:ntp/ntp.dart';
import 'package:product_time_zone/model/product_model.dart';

class AppDateHelper {
  static Future<DateTime> _getRealTimeFromProductTime(
      {@required DateTime productTime, @required DateTime cuurentTime}) async {
    // print(_currentTime);

    Duration offset = cuurentTime.timeZoneOffset;

    // print("current time offset $offset");
    // print("Product time offset ${productTime.timeZoneOffset}");

    DateTime realTime = productTime.add(offset);

    // print("Real Time -----------> ${realTime}");
    return realTime;
  }

  static Future<Duration> getDiffranceBetweenStartDateAndCurrentTime(
      ProductModel productModel,
      {DateTime cuurentTime}) async {
    // DateTime _currentTime = await _currentDateTime();
    print("current time --------------> ${cuurentTime}");

    DateTime startDateTime = await _getRealTimeFromProductTime(
        productTime: DateTime.parse(productModel.startAt).toUtc(),
        cuurentTime: cuurentTime);

    print("start time -------------> ${startDateTime}");
    Duration duration = diffrance(startDateTime, cuurentTime);
    print("Start to current ${duration.inHours}");
    return duration;
  }

  static Future<Duration> getDiffranceBetweenEndDateAndCurrentTime(
      ProductModel productModel,
      {DateTime currentTime}) async {
    // DateTime _currentTime = await _currentDateTime();
    // print("current time --------------> ${_currentTime}");

    //utc time
    DateTime startDate = DateTime.parse(productModel.startAt);
    DateTime endDate = DateTime.parse(productModel.endAt);
    //
    Duration startToEndDiff = endDate.difference(startDate);
    DateTime fullEndDate = endDate;
    if (startToEndDiff.inHours <= 0 &&
        startToEndDiff.inMinutes <= 0 &&
        startToEndDiff.inSeconds <= 0) {
      fullEndDate = endDate.add(Duration(hours: 1));
    } else {
      fullEndDate = endDate.add(startToEndDiff);
    }

    DateTime endDateTime = await _getRealTimeFromProductTime(
        productTime: fullEndDate.toUtc(), cuurentTime: currentTime);
    // return diffrance(endDateTime, _currentTime);

    Duration duration = diffrance(endDateTime, currentTime);
    print("end to current ${duration.inHours}");
    return duration;
  }

  static Duration diffrance(DateTime first, DateTime second) {
    DateTime newFirst = DateTime(first.year, first.month, first.day, first.hour,
        first.minute, first.second, first.millisecond, first.microsecond);
    DateTime newSecond = DateTime(
        second.year,
        second.month,
        second.day,
        second.hour,
        second.minute,
        second.second,
        second.millisecond,
        second.microsecond);

    Duration duration = newFirst.difference(newSecond);
    if (duration.isNegative) {
      return Duration();
    } else {
      return duration;
    }
  }

  static String getFormatedDate(Duration diffrance) {
    if (diffrance.inSeconds > 0) {
      final days = diffrance.inDays;
      final hours = diffrance.inHours - diffrance.inDays * 24;
      final minutes = diffrance.inMinutes - diffrance.inHours * 60;
      final seconds = diffrance.inSeconds - diffrance.inMinutes * 60;
      return '$days Day: $hours Hours: $minutes Minutes ';
    } else {
      return '0 Day: 0 Hours: 0 Minutes';
    }
  }

  static Future<DateTime> currentDateTime() async {
    return await NTP.now();
  }
}
