import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:product_time_zone/dao/product_dao.dart';
import 'package:product_time_zone/elements/app_btn.dart';
import 'package:product_time_zone/elements/app_textfileds.dart';
import 'package:product_time_zone/elements/date_picker.dart';
import 'package:product_time_zone/elements/spacer.dart';
import 'package:product_time_zone/model/product_model.dart';

class CreateProductPage extends StatefulWidget {
  @override
  _CreateProductPageState createState() => _CreateProductPageState();
}

class _CreateProductPageState extends State<CreateProductPage> {
  TextEditingController _nameCtrl = TextEditingController();
  DateTime _selectedStartDate;

  DateTime _selectedEndDate;

  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
            onPressed: () {
              Get.back();
            }),
        title: Text(
          "New Product",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(
        children: [
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  textFiled(
                      hintText: "Product Name",
                      ctrl: _nameCtrl,
                      validation: requiredValidation),
                  spacerH(25),
                  datePickerItem(
                      dateTime: _selectedStartDate,
                      onDatePicked: (DateTime date) {
                        setState(() {
                          _selectedStartDate = date;
                        });
                      },
                      title: "Start Date"),
                  spacerH(25),
                  datePickerItem(
                      onDatePicked: (DateTime date) {
                        setState(() {
                          _selectedEndDate = date;
                        });
                      },
                      title: "End Date",
                      dateTime: _selectedEndDate),
                  spacerH(25),
                  appBtn(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          _createNewProduct();
                        }
                      },
                      w: Get.width / 1,
                      h: null,
                      text: "CREATE")
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _createNewProduct() async {
    print(
        "TIME ZONE OFFSET -------------> ${_selectedStartDate.toUtc().timeZoneOffset}");
    print("TIME ZONE OFFSET -------------> ${_selectedStartDate.toUtc()}");
    print("TIME ZONE OFFSET -------------> ${_selectedStartDate}");

    if (_selectedStartDate == null) {
      Get.snackbar("message", "Start Date Required");
      return;
    }
    if (_selectedEndDate == null) {
      Get.snackbar("message", "End Date Required");
      return;
    }
    final productDao = ProductDao();
    String productNameFromCtrl = getTextFromCtrl(_nameCtrl);
    await productDao.saveProduct(ProductModel(
        productName: productNameFromCtrl,
        startAt: _selectedStartDate.toUtc().toIso8601String(),
        endAt: _selectedEndDate.toUtc().toIso8601String()));
    Get.back();
  }
}
