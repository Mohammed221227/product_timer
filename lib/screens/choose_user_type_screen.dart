import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:product_time_zone/core/enums/enums.dart';
import 'package:product_time_zone/elements/spacer.dart';
import 'package:product_time_zone/screens/state/app_state.dart';
import 'package:provider/provider.dart';

import 'all_products_screen.dart';

class ChooseUserPage extends StatefulWidget {
  @override
  _ChooseUserPageState createState() => _ChooseUserPageState();
}

class _ChooseUserPageState extends State<ChooseUserPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                spacerH(100),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(
                        "Choose User type",
                        style: TextStyle(fontSize: 22),
                      ),
                    ],
                  ),
                ),
                spacerH(25),
                Row(
                  children: [
                    Flexible(
                        child: userType(
                            icon: Icons.group,
                            title: "Buyer",
                            onTap: () {
                              context
                                  .read<AppState>()
                                  .setUserType(UserType.Buyer);
                              Get.offAll(AllProductsScreen());
                            })),
                    Flexible(
                        child: userType(
                            icon: Icons.storefront,
                            title: "Seller",
                            onTap: () {
                              context
                                  .read<AppState>()
                                  .setUserType(UserType.Seller);

                              Get.offAll(AllProductsScreen());
                            })),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget userType(
      {@required String title,
      @required IconData icon,
      @required Function onTap}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: 200,
          height: 130,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(
              child: Column(
                children: [
                  Icon(
                    icon,
                    size: 100,
                  ),
                  Text(title)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
