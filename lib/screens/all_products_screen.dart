import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:product_time_zone/dao/product_dao.dart';
import 'package:product_time_zone/elements/spacer.dart';
import 'package:product_time_zone/model/product_model.dart';
import 'package:product_time_zone/screens/state/app_state.dart';
import 'package:product_time_zone/widgets/product_item.dart';
import 'package:provider/provider.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';

import 'create_product_screen.dart';

class AllProductsScreen extends StatefulWidget {
  @override
  _AllProductsScreenState createState() => _AllProductsScreenState();
}

class _AllProductsScreenState extends State<AllProductsScreen> {
  @override
  Widget build(BuildContext context) {
    return context.watch<AppState>().isBuyer()
        ? Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 1,
            ),
            body: ListView(
              children: [_productsList(), spacerH(200)],
            ),
          )
        : Scaffold(
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                Get.to(CreateProductPage());
              },
            ),
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 1,
              title: Text("Products"),
            ),
            body: ListView(
              children: [_productsList(), spacerH(200)],
            ),
          );
  }

  Widget _productsList() {
    var productDao = ProductDao();
    return FirebaseAnimatedList(
      query: productDao.getProductQuery(),
      reverse: true,
      primary: false,
      shrinkWrap: true,
      itemBuilder: (context, snapshot, animation, index) {
        final json = snapshot.value as Map<dynamic, dynamic>;
        final productModel = ProductModel.fromJson(json);
        return ProductItem(
          productModel: productModel,
        );
      },
    );
  }
}
