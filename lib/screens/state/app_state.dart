import 'package:flutter/cupertino.dart';
import 'package:product_time_zone/core/enums/enums.dart';

class AppState extends ChangeNotifier {
  UserType _userType;

  setUserType(UserType userType) {
    _userType = userType;
    notifyListeners();
  }

  UserType get userType => _userType;

  bool isBuyer() {
    return this._userType == UserType.Buyer;
  }
}
