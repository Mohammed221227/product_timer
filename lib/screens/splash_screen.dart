import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ntp/ntp.dart';
import 'package:product_time_zone/res.dart';

import 'choose_user_type_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("Timezone ----------------> ${DateTime.now().timeZoneName}");
    print("Timezone ----------------> ${DateTime.now().timeZoneOffset}");

    waitForThreeSec(onDone: () {
      Get.to(ChooseUserPage());
    });
  }

  waitForThreeSec({@required Function onDone}) {
    Future.delayed(Duration(seconds: 3), () {
      onDone();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height / 3,
          ),
          Image.asset(
            Res.logo,
            width: 200,
            height: 200,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [CircularProgressIndicator()],
            ),
          )
        ],
      ),
    );
  }
}
